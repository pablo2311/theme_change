<?php
/**
 * Register widget areas
 *
 * @package Tulbuz
 * @since Tulbuz 1.0.0
 */

if ( ! function_exists( 'Tulbuz_sidebar_widgets' ) ) :
	function Tulbuz_sidebar_widgets() {
		register_sidebar(
			array(
				'id'            => 'sidebar-widgets',
				'name'          => __( 'Sidebar widgets', 'Tulbuz' ),
				'description'   => __( 'Drag widgets to this sidebar container.', 'Tulbuz' ),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h6>',
				'after_title'   => '</h6>',
			)
		);

		register_sidebar(
			array(
				'id'            => 'footer-widgets',
				'name'          => __( 'Footer widgets', 'Tulbuz' ),
				'description'   => __( 'Drag widgets to this footer container', 'Tulbuz' ),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h6>',
				'after_title'   => '</h6>',
			)
		);
	}

	add_action( 'widgets_init', 'Tulbuz_sidebar_widgets' );
endif;
