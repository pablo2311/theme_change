<?php
/**
 * Change the class for sticky posts to .wp-sticky to avoid conflicts with Foundation's Sticky plugin
 *
 * @package Tulbuz
 * @since Tulbuz 2.2.0
 */

if ( ! function_exists( 'Tulbuz_sticky_posts' ) ) :
	function Tulbuz_sticky_posts( $classes ) {
		if ( in_array( 'sticky', $classes, true ) ) {
			$classes   = array_diff( $classes, array( 'sticky' ) );
			$classes[] = 'wp-sticky';
		}
		return $classes;
	}
	add_filter( 'post_class', 'Tulbuz_sticky_posts' );

endif;
