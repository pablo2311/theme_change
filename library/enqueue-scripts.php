<?php

/**
 * Enqueue all styles and scripts
 *
 * Learn more about enqueue_script: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_script}
 * Learn more about enqueue_style: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_style }
 *
 * @package Tulbuz
 * @since Tulbuz 1.0.0
 */
// Check to see if rev-manifest exists for CSS and JS static asset revisioning
//https://github.com/sindresorhus/gulp-rev/blob/master/integration.md

if (!function_exists('Tulbuz_asset_path')) :

  function Tulbuz_asset_path($filename) {
    $filename_split = explode('.', $filename);
    $dir = end($filename_split);
    $manifest_path = dirname(dirname(__FILE__)) . '/dist/assets/' . $dir . '/rev-manifest.json';

    if (file_exists($manifest_path)) {
      $manifest = json_decode(file_get_contents($manifest_path), true);
    } else {
      $manifest = [];
    }

    if (array_key_exists($filename, $manifest)) {
      return $manifest[$filename];
    }
    return $filename;
  }

endif;


if (!function_exists('Tulbuz_scripts')) :

  function Tulbuz_scripts() {

    // Enqueue the main Stylesheet.
    wp_enqueue_style('main-stylesheet', get_stylesheet_directory_uri() . '/dist/assets/css/' . Tulbuz_asset_path('app.css'), array(), '2.10.4', 'all');

    // Deregister the jquery version bundled with WordPress.
    wp_deregister_script('jquery');

    // CDN hosted jQuery placed in the header, as some plugins require that jQuery is loaded in the header.
    //wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js', array(), '3.2.1', false);

    wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js', array(), '1.12.4', false);

    // Deregister the jquery-migrate version bundled with WordPress.
    wp_deregister_script('jquery-migrate');

    // CDN hosted jQuery migrate for compatibility with jQuery 3.x
    wp_register_script('jquery-migrate', '//code.jquery.com/jquery-migrate-3.0.1.min.js', array('jquery'), '3.0.1', false);

    // Enqueue jQuery migrate. Uncomment the line below to enable.
    // wp_enqueue_script( 'jquery-migrate' );

    wp_register_script('tweenMax', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.1/TweenMax.min.js', null, null, true);
//    wp_enqueue_script('tweenMax');

//    wp_register_script('scrollMagic', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.js', null, null, true);
//    wp_enqueue_script('scrollMagic');

    wp_register_script('gsap', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.js', null, null, true);
//    wp_enqueue_script('gsap');

    wp_register_script('indicators', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js', null, null, true);
//    wp_enqueue_script('indicators');


    // Enqueue Foundation scripts
    wp_enqueue_script('foundation', get_stylesheet_directory_uri() . '/dist/assets/js/' . Tulbuz_asset_path('app.js'), array('jquery'), '2.10.4', true);

    // Enqueue FontAwesome from CDN. Uncomment the line below if you need FontAwesome.
    //wp_enqueue_script( 'fontawesome', 'https://use.fontawesome.com/5016a31c8c.js', array(), '4.7.0', true );
    // Add the comment-reply library on pages where it is necessary
    if (is_singular() && comments_open() && get_option('thread_comments')) {
      wp_enqueue_script('comment-reply');
    }
  }

  add_action('wp_enqueue_scripts', 'Tulbuz_scripts');
endif;
