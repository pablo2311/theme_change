<?php
/**
 * Template part for mobile top bar menu
 *
 * @package Tulbuz
 * @since Tulbuz 1.0.0
 */

?>

<nav class="mobile-menu vertical menu" id="<?php Tulbuz_mobile_menu_id(); ?>" role="navigation">
	<?php Tulbuz_mobile_nav(); ?>
</nav>
