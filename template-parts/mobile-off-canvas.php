<?php
/**
 * Template part for off canvas menu
 *
 * @package Tulbuz
 * @since Tulbuz 1.0.0
 */

?>

<nav class="mobile-off-canvas-menu off-canvas position-left" id="<?php Tulbuz_mobile_menu_id(); ?>" data-off-canvas data-auto-focus="false" role="navigation">
	<?php Tulbuz_mobile_nav(); ?>
</nav>

<div class="off-canvas-content" data-off-canvas-content>
