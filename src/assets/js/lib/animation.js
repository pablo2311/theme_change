//import $ from 'jquery';

$(window).on('load', function () {
  /* check is home */
  
  /** end home check **/


  /** global scroll magic - add animation **/
  var ctrl = new ScrollMagic.Controller({
    globalSceneOptions: {
//      triggerHook: 'onLeave'
      reverse: false //removes the replaying of animations
    }
  });

  $(".section").each(function () {
    var $this = $(this);
    new ScrollMagic.Scene({
      triggerElement: this,
      triggerHook: 0.6,
      duration: 0
    })
//            .setPin(this) //this pins item when it hits the top
//            .addIndicators({
//              colorStart: "rgba(0,255,0,0.5)",
//              colorEnd: "rgba(0,0,255,0.5)",
//              colorTrigger: "rgba(255,0,0,1)",
//              name: name
//            })
            .removeClassToggle(false)
            .setClassToggle(this, "is-active")
//            .setTween(tween)
//            .loglevel(3)
            .addTo(ctrl);
  });

  $(".sticky-container").each(function () {
    var $this = $(this);
    new ScrollMagic.Scene({
      triggerElement: this,
      triggerHook: 0.6,
      duration: 0
    })
//            .setPin(this) //this pins item when it hits the top
//            .addIndicators({
//              colorStart: "rgba(0,255,0,0.5)",
//              colorEnd: "rgba(0,0,255,0.5)",
//              colorTrigger: "rgba(255,0,0,1)",
//              name: name
//            })
            .removeClassToggle(false)
            .setClassToggle(this, "is-active")
//            .setTween(tween)
//            .loglevel(3)
            .addTo(ctrl);
  });


});
