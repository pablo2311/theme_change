jQuery(function () {


  jQuery(".design-carousel").owlCarousel({
    items: 2,
//          stagePadding: 100,
    margin: 22,
    nav: true,
//    dots: true,
    loop: true,
    dotsData: true,
    dotData: true,
    lazyLoad: true,
    dotsContainer: ".designs-dots-container",
    navText: ["<i class='icon-left-arrow'></i>", "<i class='icon-right-arrow'></i>"],
//    navContainer: ".how-to-nav-container",
//    onInitialized: counter,
//     onTranslated : counter, //When the translation of the stage has finished.
    responsive: {
      0: {
        items: 1,
        stagePadding: 15,
        loop: true,
        margin: 10
//        dots: true,
      },
      640: {
        items: 1,
        stagePadding: 0,
      },
      1024: {
        margin: 30,
        items: 1,
//        stagePadding: 50,
      }
    }
  });
  
  jQuery(".inclusions-carousel").owlCarousel({
    items: 2,
//          stagePadding: 100,
    margin: 22,
    nav: true,
    lazyLoad: true,
//    dots: true,
    loop: true,
    dotsData: true,
    dotData: true,
    dotsContainer: ".inclusions-dots-container",
    navText: ["<i class='icon-left-arrow'></i>", "<i class='icon-right-arrow'></i>"],
//    navContainer: ".how-to-nav-container",
//    onInitialized: counter,
//     onTranslated : counter, //When the translation of the stage has finished.
    responsive: {
      0: {
        items: 1,
        stagePadding: 15,
        loop: true,
        margin: 10
//        dots: true,
      },
      640: {
        items: 1,
        stagePadding: 0,
      },
      1024: {
        margin: 30,
        items: 1,
//        stagePadding: 50,
      }
    }
  });

  jQuery('.gallery-carousel').each(function (i) {
    var navContainerN = ".carousel-" + i + " .gallery-nav-container";
    var dotsContainerN = ".carousel-" + i + " .gallery-dots-container";
//    console.log(navContainerN + "\n " + dotsContainerN);
    jQuery(this).owlCarousel({
      items: 1,
//          stagePadding: 100,
//    margin: 22,
      nav: true,
      dots: true,
      loop: false,
      lazyLoad: true,
      navText: ["<i class='icon-left-arrow'></i>", "<i class='icon-right-arrow'></i>"],
      navContainer: navContainerN,
      dotsContainer: dotsContainerN,
      responsive: {
        0: {
          items: 1,
          loop: true,
          margin: 0
        },
        640: {
          items: 1,
        },
        1024: {
          items: 1,
        }
      }
    });
  });


});

jQuery(document).ready(function () {



  jQuery('.gallery-carousel').each(function (i) {
    var dotCount = 1;
//    var $this = jQuery(this);
    jQuery('.carousel-' + i + ' .owl-dot').each(function () {
      jQuery(this).addClass('dotnumber' + dotCount);
      jQuery(this).attr('data-info', dotCount);
      dotCount = dotCount + 1;
    });

    var slidecount = 1;

    jQuery('.carousel-' + i + ' .owl-item').not('.cloned').each(function () {
      jQuery(this).addClass('slidenumber' + slidecount);
      slidecount = slidecount + 1;
    });

    jQuery('.carousel-' + i + ' .owl-dot').each(function () {

      var grab = jQuery(this).data('info');
      var slidegrab = jQuery('.carousel-' + i).find('.slidenumber' + grab + ' .gallery-image').attr('data-thumbnail');

      jQuery(this).css("background-image", "url(" + slidegrab + ")");

    });


  });




});
