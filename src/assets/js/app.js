import $ from 'jquery';
import whatInput from 'what-input';
import owlCarousel from 'owl.carousel';

//import ScrollMagic from 'scrollmagic';
//import 'imports?define=>false!scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';

import ScrollMagic from 'ScrollMagic';
import 'animation.gsap';
import 'debug.addIndicators';

window.$ = $;
//import ScrollMagic from 'ScrollMagic';
//import 'animation.gsap';
//import 'debug.addIndicators';
import Foundation from 'foundation-sites';
// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
//import './lib/foundation-explicit-pieces';

$(document).foundation();

import './lib/owl-carousel-init';

import './lib/animation';

jQuery(document).ready(function(){
  jQuery('body').addClass('loaded');
});

/** autoplay looped video **/
/*if(jQuery("#video").length !== 0){
  jQuery(document).ready(function(){
    jQuery("#video").get(0).play();
  });

  
  var youtubeIframe = jQuery('.youtube-video');
  var youtubeIframeSrc = youtubeIframe[0].src;

  jQuery('[data-reveal]').on('open.zf.reveal', function() {
    var youtubeOpen = youtubeIframeSrc + "&autoplay=1";
    youtubeIframe.attr('src', youtubeOpen);

  });
  
  jQuery('[data-reveal]').on('closed.zf.reveal', function() {  
    var youtubeClose = youtubeIframeSrc + "&autoplay=0";
    youtubeIframe.attr('src', youtubeClose);
  });

}*/

$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    event.preventDefault();
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top -120
        }, 400, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });
  
  
  
  $('.scroll-down').on('click',function(){
    $('html, body').animate({
          scrollTop: jQuery('.g-intro').offset().top -30
        }, 400);
  });